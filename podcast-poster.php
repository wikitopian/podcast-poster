<?php
/*
Plugin Name: Podcast Poster
Plugin URI: https://bitbucket.org/wikitopian/podcast-poster/
Description: Allow authors to interactively edit podcasts as they're uploading them.
Version: 0.1
Author: Matt Parrott
Author URI: http://www.swarmstrategies.com/matt
License: GPLv2
 */

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

class PodcastPoster {

	public function __construct() {
		register_activation_hook( __FILE__, array( &$this, 'do_activation' ) );
		register_deactivation_hook( __FILE__, array( &$this, 'do_deactivation' ) );
		register_uninstall_hook( __FILE__, array( &$this, 'do_uninstall' ) );
		add_action( 'init', array( &$this, 'do_init' ) );
		add_action( 'init', array( &$this, 'set_post_type' ) );
	}
	public function do_activation() {
		if ( !get_option( 'podcast-poster' ) ) {
			$settings = array();
			$settings['sox_path']  = '/usr/bin/sox';
			$settings['db_prefix'] = 'podcast_poster_';
			update_option( 'podcast-poster', $settings );
		}
		$this->add_tables();
	}
	private function add_tables() {
		global $wpdb;
		$settings  = get_option( 'podcast-poster' );
		$db_prefix = $settings['db_prefix'];
		$db_prefix = $wpdb->prefix . $db_prefix;

		$sql_segment = <<<SQL
CREATE TABLE {$db_prefix}segment (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	project_id mediumint(9) NOT NULL,
	status tinytext NOT NULL,
	label text NOT NULL,
	url varchar(120) DEFAULT '' NOT NULL,
	seconds int(11) NOT NULL,
	quality double NOT NULL,
	UNIQUE KEY id (id)
);
SQL;
		dbDelta( $sql_segment );

		$sql_scrap = <<<SQL
CREATE TABLE {$db_prefix}scrap (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	segment_id mediumint(9) NOT NULL,
	url varchar(120) DEFAULT '' NOT NULL,
	first int(11) NOT NULL,
	final int(11) NOT NULL,
	UNIQUE KEY id (id)
);
SQL;
		dbDelta( $sql_scrap );


		$sql_effect = <<<SQL
CREATE TABLE {$db_prefix}effect (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	segment_id mediumint(9) NOT NULL,
	first int(11) NOT NULL,
	final int(11) NOT NULL,
	effect varchar(50) DEFAULT '' NOT NULL,
	UNIQUE KEY id (id)
);
SQL;
		dbDelta( $sql_effect );

	}
	public function do_deactivation() {
	}
	public function do_uninstall() {
		delete_option( 'podcast-poster' );
		del_table( 'segment' );
	}
	private function del_table( $table ) {
		global $wpdb;
		$settings  = get_option( 'podcast-poster' );
		$db_prefix = $settings['db_prefix'];
		$db_prefix = $wpdb->prefix . $db_prefix;

		$sql = "DROP TABLE {$table}{$table};";
		dbDelta( $sql );
	}
	public function do_init() {

		wp_register_script(
			'jquery_file_upload',
			plugins_url( 'jQuery-File-Upload/js/jquery.fileupload.js', __FILE__ ),
			array( 'jquery' )
		);

		wp_register_script(
			'jquery_file_upload_ui',
			plugins_url( 'jQuery-File-Upload/js/jquery.fileupload-ui.js', __FILE__ ),
			array( 'jquery-ui-widget' )
		);

		wp_register_style(
			'jquery_file_upload_ui',
			plugins_url( 'jQuery-File-Upload/css/jquery.fileupload-ui.css', __FILE__ )
		);

		wp_register_script(
			'podcast_poster',
			plugins_url( 'js/podcast-poster.js', __FILE__ )
		);

		wp_register_style(
			'podcast_poster',
			plugins_url( 'style/podcast-poster.css', __FILE__ )
		);

		wp_register_script(
			'podcast_poster_upload',
			plugins_url( 'js/podcast-poster-upload.js', __FILE__ )
		);

		add_action( 'admin_print_styles', array( &$this, 'do_styles' ) );
		add_action( 'admin_print_scripts', array( &$this, 'do_scripts' ) );

		add_action( 'admin_init', array( &$this, 'do_settings' ) );
		add_action( 'admin_menu', array( &$this, 'do_settings_menu' ) );
	}
	public function do_scripts() {
		global $typenow;
		if( $typenow == 'podcast-poster' ) {
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'jquery-ui-widget' );
			wp_enqueue_script( 'jquery_file_upload' );
			wp_enqueue_script( 'jquery_file_upload_ui' );
			wp_enqueue_script( 'podcast_poster' );
			wp_enqueue_script( 'podcast_poster_upload' );
		}
	}
	public function do_styles() {
		global $typenow;
		if( $typenow == 'podcast-poster' ) {
			wp_enqueue_style( 'podcast_poster' );
			wp_enqueue_style( 'jquery_file_upload_ui' );
		}
	}
	function set_post_type() {
		register_post_type(
			'podcast-poster',
			array(
				'labels' => array(
					'name' => 'Podcasts',
					'singular_name' => 'Podcast',
					'add_new' => 'Add New Podcast',
					'add_new_item' => 'Add New Podcast',
					'new_item' => 'Add New Podcast',
					'edit_item' => 'Edit Podcast',
					'view_item' => 'View Podcast',
					'search_items' => 'Search Podcasts',
					'not_found' => 'No Podcasts found',
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => plugins_url( 'images/podcast-poster-16.png', __FILE__ ),
				'menu_position' => 2,
				'supports' => array(
					'title',
					'editor',
					'author',
					'thumbnail',
					'comments',
					'revisions',
				),
				'register_meta_box_cb' => array( &$this, 'set_metabox' )
			)
		);
	}
	public function set_metabox() {
		add_meta_box(
			'podcast-poster',
			'Podcast Poster',
			array( &$this, 'set_metabox_content' ),
			'podcast-poster',
			'normal',
			'high'
		);
	}
	public function set_metabox_content( $post ) {
		require( 'includes/jquery-file-uploader-box.php' );
	}
	public function do_settings() {
		register_setting( 'podcast-poster', 'podcast-poster' );
	}
	public function do_settings_menu() {
		add_options_page(
			'Podcast Settings',
			'Podcast Settings',
			'manage_options',
			'podcast_poster_settings_page',
			array( &$this, 'do_settings_menu_page' )
		);
	}
	public function do_settings_menu_page() {
		require( 'includes/settings_menu_page.php' );
	}
}
$podcast_poster = new PodcastPoster();
