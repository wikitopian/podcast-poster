# Podcast Poster
Allow authors to interactively edit podcasts as they're uploading them. Podcast Poster relies on ajax communication with the `sox` utility, which must be installed on your hosting account.

## Description
* Podcast is a *Custom Post Type*
* Upload and edit multiple audio files into one
* Trim, balance, and even splice in WordPress

## License
GPLv2

## Installation

1. Install the plugin

2. Increase the WordPress memory limit

### Add `define('WP_MEMORY_LIMIT', '512M');` to the top of `wp-config.php`

### Add `php_value  upload_max_filesize  512` top the bottom of `.htaccess`

### Add or modify `upload_max_filesize = 512M` and `max_post_size = 512M` in `php.ini`

## Contact & Feedback
I, ([Matt Parrott](http://www.swarmstrategies.com)), conceptualized and designed it.

Please do hit me up for bugfixes and support!
