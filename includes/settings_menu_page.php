<?php

if( !function_exists( 'current_user_can' ) || !current_user_can( 'manage_options' ) ) {
	die( 'Security Fail' );
}

?>

<div class="wrap">
	<div class="icon32" id="icon-options-general"><br /></div>
	<h2>Podcast Poster Settings</h2>

	<form name="podcast-poster" method="post" action="options.php">
		<?php settings_fields( 'podcast-poster' ); ?>
		<?php //do_settings( 'podcast-poster' ); ?>
		<?php $settings = get_option( 'podcast-poster' ); ?>

		<table class="form-table">
			<tr valign="top">
				<th scope="row">
					<label for="sox_path">Sox Path</label>
				</th>
				<td>
					<input
						 name="podcast-poster[sox_path]"
						 type="text"
						 value="<?php echo $settings['sox_path']; ?>"
						 class="regular-text" />
				</td>
			</tr>
		</table>

		<?php submit_button(); ?>
	</form>
</div>
